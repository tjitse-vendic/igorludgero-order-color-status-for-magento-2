/**
 * Created by igorludgeromiura on 25/09/16.
 */

require(["jquery"], function($){

    $(document).ready(function ()
    {

        var i = setInterval(function ()
        {
            if ($('.statusCSScolor').length)
            {
                //clearInterval(i);
                $('.statusCSScolor').each(function(i, obj) {
                    var classStatusName = $(this).children().text();
                    classStatusName = classStatusName.toString().replace(" ","_").toLowerCase();
                    //console.log(classStatusName);
                    $(this).parent().attr("class","");
                    $(this).parent().addClass('data-row');
                    $(this).parent().addClass(classStatusName);
                    if($(this).parent().hasClass(classStatusName)) {
                        $(this).removeClass('statusCSScolor');
                    }
                });
            }
        }, 100);
    });

});
