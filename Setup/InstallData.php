<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 23/09/16
 * Time: 14:47
 */

namespace Igorludgero\Colorstatus\Setup;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $tableName = $installer->getTable('sales_order_status');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) == true){
            $installer->getConnection()->addColumn(
                $installer->getTable('sales_order_status'),
                'color',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'unsigned' => true,
                    'nullable' => true,
                    'default' => '',
                    'comment' => 'Color Column'
                ]
            );
        }
    }
}