<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 23/09/16
 * Time: 15:47
 */

namespace Igorludgero\Colorstatus\Block\Adminhtml\Order\Status\Edit;

class Form extends \Igorludgero\Colorstatus\Block\Adminhtml\Order\Status\NewStatus\Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('new_order_status');
    }

    protected function _prepareForm()
    {
        parent::_prepareForm();
        $form = $this->getForm();
        $form->getElement('base_fieldset')->removeField('is_new');
        $form->getElement('base_fieldset')->removeField('status');
        $form->setAction(
            $this->getUrl('sales/order_status/save', ['status' => $this->getRequest()->getParam('status')])
        );
        return $this;
    }
}
