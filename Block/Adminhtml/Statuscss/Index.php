<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 26/09/16
 * Time: 11:51
 */

namespace Igorludgero\Colorstatus\Block\Adminhtml\Statuscss;

class Index extends \Magento\Backend\Block\Template
{

    protected $_status;
    protected $_logger;

    public function getStatusCss()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_status = $objectManager->get('\Magento\Sales\Model\Order\Status');
        $this->_logger = $objectManager->get('\Igorludgero\Colorstatus\Logger\Logger');
        $statusCollection = $this->_status->getCollection();
        $arrayCSS = array();
        foreach ($statusCollection as $status){
            if($status->getColor()) {
                $lowerStatus = strtolower($status->getStatus());
                $statusFormated = str_replace(" ", "_", $lowerStatus);
                $arrayCSS[] = "." . $statusFormated . " { background-color: #" . $status->getColor() . " }";
            }
        }
        $stringFinal='<style type="text/css">';
        foreach ($arrayCSS as $css){
            $stringFinal = $stringFinal.$css;
        }
        $stringFinal = $stringFinal.'</style>';
        //$this->_logger->info($stringFinal);
        return $stringFinal;
    }


}