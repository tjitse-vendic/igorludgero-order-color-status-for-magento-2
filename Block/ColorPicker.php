<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 23/09/16
 * Time: 16:53
 */

namespace Igorludgero\Colorstatus\Block;

class ColorPicker extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array                                   $data
     */
    public function __construct(\Magento\Backend\Block\Template\Context $context, array $data = []){
        parent::__construct($context, $data);
    }

    /**
     * add color picker in admin configuration fields
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string script
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->addClass("jscolor");
        $html = $element->getElementHtml();
        return $html;
    }
}